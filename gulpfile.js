'use strict';

let gulp = require('gulp'),
      sass = require('gulp-sass'),
      browserSync = require('browser-sync').create(),
      cssnano = require('gulp-cssnano'),
      imagemin = require('gulp-imagemin'),
      autoprefixer = require('autoprefixer'),
      cssbeautify = require('gulp-cssbeautify');
/*autoprefixer support*/
let supported = [
    'last 2 versions',
    'safari >= 8',
    'ie >= 10',
    'ff >= 20',
    'ios 6',
    'android 4'
];

/*css unmin*/
  gulp.task('uncssmin', () => {
    return gulp.src('app/css/style.css')
        .pipe(cssbeautify())
        .pipe(gulp.dest('app/css/expanded'))
        .pipe(browserSync.reload({
            stream:true
        }))
});
/*sass compilataion*/
gulp.task('sass' , () => {
     return gulp.src(['app/scss/**/*.scss'])
    .pipe(sass({noCache: true }))
    .pipe(cssnano({
        autoprefixer: {browsers: supported, add: true}
    }))
    .pipe(gulp.dest('app/css/'))
    .pipe(browserSync.reload({
        stream:true
    }))
});
/*image min*/
gulp.task('imagemin', () =>
gulp.src('app/images/*')
    .pipe(imagemin([
        imagemin.gifsicle({interlaced: true}),
        imagemin.jpegtran({progressive: true}),
        imagemin.optipng({optimizationLevel: 12}),
        imagemin.svgo({
            plugins: [
                {removeViewBox: true},
                {cleanupIDs: false}
            ]
        })
    ]))
    .pipe(gulp.dest('app/minimages/'))
);
/*browser sync start*/
gulp.task('browserSync', () => {
    browserSync.init({
      server: {
        baseDir: 'app'
      },
    })
  })
  gulp.task('watch', ['browserSync','sass','imagemin','uncssmin'],  () => {
    gulp.watch('app/scss/**/*.scss', ['sass']); 
    gulp.watch('app/css/expanded/*.css', browserSync.reload);
    gulp.watch('app/*.html', browserSync.reload); 
    gulp.watch('app/js/**/*.js', browserSync.reload); 
  })

